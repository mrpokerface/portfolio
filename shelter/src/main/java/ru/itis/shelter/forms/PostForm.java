package ru.itis.shelter.forms;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.itis.shelter.models.Animal;

import java.sql.Timestamp;

@Setter
@NoArgsConstructor
@AllArgsConstructor
public class PostForm {

    @Getter
    private String desc;
    private String startDate;
    private String endDate;

    public Timestamp getStartDate() {
        return Timestamp.valueOf(startDate + " 00:00:00");
    }

    public Timestamp getEndDate() {
        return Timestamp.valueOf(endDate + " 00:00:00");
    }
}
