package ru.itis.shelter.services;

import ru.itis.shelter.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.PostsRepositoryHibernateImpl;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class PostService {

    @Autowired
    private PostsRepositoryHibernateImpl postsRepository;

    public List<Post> getAllPosts() {
        List<Post> list = postsRepository.findAll();
        List<Post> actualList = new ArrayList<>();
        for(Post post : list) {
            if(!post.isFinished()) {
                actualList.add(post);
            }
        }
        return actualList;
    }

    public Optional<Post> getPost(int id) {
        return postsRepository.findOne(id);
    }

    public void addPost(Post post) {
        postsRepository.save(post);
    }

    public void markAsDone(Post post) {
        postsRepository.markAsDone(post);
    }

}