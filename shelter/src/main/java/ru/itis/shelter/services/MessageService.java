package ru.itis.shelter.services;

import ru.itis.shelter.models.Message;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.MessageRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

@Service
public class MessageService {

    @Autowired
    private MessageRepositoryImpl messageRepository;

    public List<Message> getByIdsLegit(User user, int id) {
        List<Message> messages = messageRepository.getByIdsLegit(user.getId(), id);
        sort(messages);
        return messages;
    }

    private void sort(List<Message> list) {
        list.sort((o1, o2) -> {
                    if (o1.getId() > o2.getId()) {
                        return 1;
                    } else if (o1.getId().equals(o2.getId())) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
        );
    }

    public void sendMessage(Message message) {
        messageRepository.save(message);
    }

    public List<User> getDialogues(Integer id) {
        return messageRepository.getDialoguesOf(id);
    }
}
