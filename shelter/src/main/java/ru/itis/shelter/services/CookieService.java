package ru.itis.shelter.services;

import org.springframework.stereotype.Service;

import javax.servlet.http.Cookie;

@Service
public class CookieService {

    public Cookie getRememberMeCookie(Cookie[] cookies) {
        for(Cookie cookie : cookies) {
            if(cookie.getName().equals("remember")) {
                return cookie;
            }
        }
        return null;
    }
}
