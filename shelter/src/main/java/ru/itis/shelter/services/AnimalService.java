package ru.itis.shelter.services;

import ru.itis.shelter.models.Animal;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.AnimalRepositoryHibernateImpl;

import java.util.List;
import java.util.Optional;

@Service
public class AnimalService {

    @Autowired
    private AnimalRepositoryHibernateImpl animalRepository;

    public Animal getByAnimalId(int id) {
        Optional<Animal> animalOptional = animalRepository.findOne(id);
        return animalOptional.isPresent() ? animalOptional.get() : null;
    }

    public List<Animal> getByOwnerId(Integer id) {
        return animalRepository.getByOwnerId(id);
    }

    public void addAnimal(Animal animal) {
        animalRepository.save(animal);
    }
}
