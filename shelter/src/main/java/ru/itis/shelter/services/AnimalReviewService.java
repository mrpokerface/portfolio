package ru.itis.shelter.services;

import ru.itis.shelter.models.AnimalReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.AnimalReviewRepositoryHibernateImpl;

import java.util.List;

@Service
public class AnimalReviewService {

    @Autowired
    private AnimalReviewRepositoryHibernateImpl animalReviewRepository;

    public void addReview(AnimalReview review) {
        animalReviewRepository.save(review);
    }

    public List<AnimalReview> getReviewsByAnimalId(int id) {
        return animalReviewRepository.getAllByAnimalId(id);
    }
}
