package ru.itis.shelter.services;


import ru.itis.shelter.forms.LoginForm;
import ru.itis.shelter.forms.RegisterForm;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.UsersRepositoryHibernateImpl;

import java.util.Optional;

@Service
public class UserService {

    @Autowired
    private PasswordEncoder encoder;
    @Autowired
    private UsersRepositoryHibernateImpl usersRepository;

    public void addUser(RegisterForm form) {
        User user = User.builder()
                .login(form.getUsername())
                .hashPassword(encoder.encode(form.getPassword()))
                .name(form.getName())
                .surname(form.getSurname())
                .confirmed(false)
                .email(form.getEmail())
                .build();
        usersRepository.save(user);
    }


    public Optional<User> authenticate(LoginForm form) {
        Optional<User> userOptional = usersRepository.findByNickname(form.getUsername());
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            if (encoder.matches(form.getPassword(), user.getHashPassword())) {
                 return Optional.of(user);
            }
        }
        return Optional.empty();
    }

    public Optional<User> getById(int id) {
        return usersRepository.findOne(id);
    }
}
