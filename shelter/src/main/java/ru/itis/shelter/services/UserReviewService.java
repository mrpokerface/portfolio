package ru.itis.shelter.services;

import ru.itis.shelter.models.UserReview;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.itis.shelter.repositories.UserReviewRepositoryHibernateImpl;

import java.util.List;

@Service
public class UserReviewService {

    @Autowired
    private UserReviewRepositoryHibernateImpl reviewRepository;

    public List<UserReview> getReviewsByUserId(Integer id) {
        return reviewRepository.getByUserId(id);
    }

    public void addReview(UserReview review, int id) {
        review.setReviewedId(id);
        reviewRepository.save(review);
    }
}
