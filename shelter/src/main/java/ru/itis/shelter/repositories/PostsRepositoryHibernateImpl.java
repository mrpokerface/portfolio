package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.shelter.models.Post;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class PostsRepositoryHibernateImpl implements PostsRepository {
    @Override
    public Optional<Post> findOne(Integer id) {
        return Optional.of(
                HibernateSessionFactoryUtil
                        .getSessionFactory()
                        .openSession()
                        .get(Post.class, id)
        );
    }

    @Override
    public List<Post> findAll() {
        return (List<Post>) HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from Post")
                .list();
    }

    @Override
    public void delete(Integer id) {

    }

    public void markAsDone(Post post) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.update(post);
        tx.commit();
        session.close();
    }
}
