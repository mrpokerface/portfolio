package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.hibernate.query.Query;
import ru.itis.shelter.models.UserReview;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class UserReviewRepositoryHibernateImpl implements UserReviewRepository {
    @Override
    public Optional<UserReview> findOne(Integer id) {
        return Optional.empty();
    }

    @Override
    public List<UserReview> findAll() {
        return null;
    }

    public List<UserReview> getByUserId(Integer id) {
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from UserReview where id = " + id)
                .list();
    }

    @Override
    public void delete(Integer id) {

    }
}
