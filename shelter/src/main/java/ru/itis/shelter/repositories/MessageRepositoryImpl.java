package ru.itis.shelter.repositories;

import ru.itis.shelter.models.Message;
import ru.itis.shelter.models.User;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;
import ru.itis.shelter.services.UserService;

import javax.sql.DataSource;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

@Repository
public class MessageRepositoryImpl implements MessageRepository {

    private JdbcTemplate jdbcTemplate;
    private DataSource dataSource;

    //language=SQL
    private static final String SQL_SELECT_BY_IDS_QUERY_LEGIT =
            "select messages.id, u.name, messages.text from messages " +
                    "join users u on messages.sender_id = u.id " +
                    "where sender_id = ? and receiver_id = ? " +
                    "union select messages.id, u2.name, messages.text from messages " +
                    "join users u2 on messages.sender_id = u2.id " +
                    "where receiver_id = ? and sender_id = ?";
    //language=SQL
    private static final String SQL_INSERT_QUERY =
            "insert into messages (text, sender_id, receiver_id) VALUES (?, ?, ?)";
    //language=SQL
    private static final String SQL_SELECT_DIALOGUES_QUERY =
            "select ids.id, name from(select distinct sender_id as id from messages where receiver_id = ? " +
                    "union select distinct receiver_id from messages where sender_id = ?) " +
                    "as ids join users on ids.id = users.id";

    private RowMapper<Message> messageWithUsernameRowMapper = (resultSet, i) -> Message.builder()
            .id(resultSet.getInt("id"))
            .text(resultSet.getString("text"))
            .sender(User.builder().name(resultSet.getString("name")).build())
            .build();

    private RowMapper<User> idRowMapper = (resultSet, i) -> User.builder()
            .name(resultSet.getString("name"))
            .id(resultSet.getInt("id"))
            .build();

    public MessageRepositoryImpl(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
        this.dataSource = dataSource;
    }

    @Override
    public Optional<Message> findOne(Integer id) {
        return Optional.empty();
    }

    @Override
    public List<Message> findAll() {
        return null;
    }

    @Override
    public void save(Message model) {
        try {
            PreparedStatement preparedStatement = dataSource
                    .getConnection()
                    .prepareStatement(SQL_INSERT_QUERY, Statement.RETURN_GENERATED_KEYS);
            preparedStatement.setString(1, model.getText());
            preparedStatement.setLong(2, model.getSender().getId());
            preparedStatement.setLong(3, model.getReceiver().getId());
            preparedStatement.execute();
            ResultSet resultSet = preparedStatement.getGeneratedKeys();
            resultSet.next();
            model.setId(resultSet.getInt("id"));
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(Integer id) {

    }

    public List<Message> getByIdsLegit(Integer id, Integer id1) {
        return jdbcTemplate.query(SQL_SELECT_BY_IDS_QUERY_LEGIT, messageWithUsernameRowMapper, id, id1, id, id1);
    }

    public List<User> getDialoguesOf(Integer id) {
        return jdbcTemplate.query(SQL_SELECT_DIALOGUES_QUERY, idRowMapper, id, id);
    }
}
