package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public interface CrudRepository<T> {

    Optional<T> findOne(Integer id);
    List<T> findAll();

    default void save(T model) {
        Session session = HibernateSessionFactoryUtil.getSessionFactory().openSession();
        Transaction tx = session.beginTransaction();
        session.save(model);
        tx.commit();
        session.close();
    }

    void delete(Integer id);

}
