package ru.itis.shelter.repositories;

import ru.itis.shelter.models.Message;

public interface MessageRepository extends CrudRepository<Message> {
}
