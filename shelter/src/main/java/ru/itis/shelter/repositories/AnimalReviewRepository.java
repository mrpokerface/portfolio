package ru.itis.shelter.repositories;

import ru.itis.shelter.models.AnimalReview;

public interface AnimalReviewRepository extends CrudRepository<AnimalReview> {
}
