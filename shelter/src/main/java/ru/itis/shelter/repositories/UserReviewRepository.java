package ru.itis.shelter.repositories;

import ru.itis.shelter.models.UserReview;

public interface UserReviewRepository extends CrudRepository<UserReview> {
}
