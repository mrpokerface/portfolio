package ru.itis.shelter.repositories;

import ru.itis.shelter.models.Post;

public interface PostsRepository extends CrudRepository<Post> {
}
