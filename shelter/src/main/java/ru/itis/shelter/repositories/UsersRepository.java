package ru.itis.shelter.repositories;

import ru.itis.shelter.models.User;

public interface UsersRepository extends CrudRepository<User> {
}
