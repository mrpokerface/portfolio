package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.shelter.models.Animal;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class AnimalRepositoryHibernateImpl implements AnimalRepository {
    @Override
    public Optional<Animal> findOne(Integer id) {
        return Optional.of(
                HibernateSessionFactoryUtil
                        .getSessionFactory()
                        .openSession()
                        .get(Animal.class, id)
        );
    }

    @Override
    public List<Animal> findAll() {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }

    @SuppressWarnings("all")
    public List<Animal> getByOwnerId(Integer id) {
        return (List<Animal>)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from Animal where owner_id = :id")
                .setParameter("id", id)
                .list();
    }

}
