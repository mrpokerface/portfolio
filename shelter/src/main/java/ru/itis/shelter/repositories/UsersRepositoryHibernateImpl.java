package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.shelter.models.User;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class UsersRepositoryHibernateImpl implements UsersRepository {
    @Override
    public Optional<User> findOne(Integer id) {
        return Optional.of(HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .get(User.class, id));
    }

    public Optional<User> findByNickname(String username) {
        return Optional.of((User)HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from User where login = :username")
                .setParameter("username", username)
                .uniqueResult());
    }

    @Override
    public List<User> findAll() {
        return null;
    }

    @Override
    public void delete(Integer id) {

    }
}
