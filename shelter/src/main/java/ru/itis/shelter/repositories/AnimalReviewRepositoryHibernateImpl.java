package ru.itis.shelter.repositories;

import org.hibernate.Session;
import org.hibernate.Transaction;
import ru.itis.shelter.models.AnimalReview;
import ru.itis.shelter.utils.HibernateSessionFactoryUtil;

import java.util.List;
import java.util.Optional;

public class AnimalReviewRepositoryHibernateImpl implements AnimalReviewRepository {
    @Override
    public Optional<AnimalReview> findOne(Integer id) {
        return Optional.empty();
    }

    @Override
    public List<AnimalReview> findAll() {
        return null;
    }

    public List<AnimalReview> getAllByAnimalId(Integer id) {
        return HibernateSessionFactoryUtil
                .getSessionFactory()
                .openSession()
                .createQuery("from AnimalReview where id = :id")
                .setParameter("id", id)
                .list();
    }

    @Override
    public void delete(Integer id) {

    }
}
