package ru.itis.shelter.repositories;

import ru.itis.shelter.models.Animal;

public interface AnimalRepository extends CrudRepository<Animal> {
}
