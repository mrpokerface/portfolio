package ru.itis.shelter.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;
import java.util.UUID;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "users", schema = "public")
public class User {

    @Id
    @GeneratedValue(generator = "users_id_seq")
    private Integer id;
    private String name;
    private String surname;
    private String email;
    private Boolean confirmed;
    private String login;
    @Column(name = "password")
    private String hashPassword;

    @OneToMany
    @JoinTable(name = "user_reviews")
    @JoinColumn(name = "reviewed_user_id")
    private List<UserReview> reviews;
    @OneToMany
    @JoinTable(name = "animals")
    @JoinColumn(name = "owner_id")
    private List<Animal> pets;

    @Transient
    private UUID rememberUUID;
}
