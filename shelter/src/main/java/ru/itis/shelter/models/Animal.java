package ru.itis.shelter.models;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "animals", schema = "public")
public class Animal {

    @Id
    @GeneratedValue(generator = "animals_id_seq")
    private Integer id;

    @Column(name = "owner_id")
    private Integer ownerId;
    private String name;
    private Integer age;
    @Transient
    private Double rating;

    @OneToMany
    @JoinColumn(name = "reviewed_animal_id")
    @JoinTable(name = "animal_reviews")
    private List<UserReview> reviews;
}
