package ru.itis.shelter.models;

import lombok.*;

import javax.persistence.*;
import java.sql.Timestamp;

@Getter
@Setter
@EqualsAndHashCode
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "posts", schema = "public")
public class Post {

    @Id
    @GeneratedValue(generator = "posts_id_seq")
    private Integer id;
    @Column(name = "user_id")
    private Integer userId;
    private String description;
    @Column(name = "start_day")
    private Timestamp start;
    @Column(name = "end_day")
    private Timestamp end;
    @Column(name = "finished")
    private boolean isFinished;
}
