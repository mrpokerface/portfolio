package ru.itis.shelter.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "animal_reviews", schema = "public")
public class AnimalReview {

    @Id
    @GeneratedValue(generator = "animal_reviews_id_seq")
    private Integer id;
    private String text;
    @Column(name = "reviewer_id")
    private Integer authorId;
    @Column(name = "reviewed_animal_id")
    private Integer reviewedId;
    private Integer rating;
}
