package ru.itis.shelter.models;

import lombok.*;

import javax.persistence.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "user_reviews")
public class UserReview {

    @Id
    @GeneratedValue(generator = "reviews_id_seq")
    private Integer id;
    private String text;
    @Column(name = "author_id")
    private Integer authorId;
    @Column(name = "reviewed_user_id")
    private Integer reviewedId;
    private Integer rating;
}
