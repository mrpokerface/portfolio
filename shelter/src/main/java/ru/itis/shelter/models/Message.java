package ru.itis.shelter.models;

import lombok.*;

@Getter
@Setter
@Builder
@EqualsAndHashCode
@NoArgsConstructor
@AllArgsConstructor
public class Message {

    private Integer id;
    private User sender;
    private User receiver;
    private String text;
}
