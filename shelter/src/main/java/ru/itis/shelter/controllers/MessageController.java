package ru.itis.shelter.controllers;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.shelter.models.Message;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.MessageService;
import ru.itis.shelter.services.UserService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
public class MessageController {

    @Autowired
    private MessageService messageService;
    @Autowired
    private UserService userService;

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.POST)
    public String doPost(HttpServletRequest req, @PathVariable Integer id) {
        Optional<User> receiverOptional = userService.getById(id);
        if (receiverOptional.isPresent()) {
            User receiver = receiverOptional.get();
            User sender = (User) req.getSession().getAttribute("user");
            Message message = Message.builder()
                    .text(req.getParameter("text"))
                    .sender(sender)
                    .receiver(receiver)
                    .build();
            messageService.sendMessage(message);
        }
        return "redirect:/messages/" + id;
    }

    @RequestMapping(value = "/messages", method = RequestMethod.GET)
    public String getDialogues(HttpServletRequest req, ModelMap modelMap) {
        User user = (User) req.getSession().getAttribute("user");
        modelMap.addAttribute("user", user);
        List<User> users = messageService.getDialogues(user.getId());
        modelMap.addAttribute("dialogues", users);
        return "dialogues";
    }

    @RequestMapping(value = "/messages/{id}", method = RequestMethod.GET)
    public String getDialogues(HttpServletRequest req, @PathVariable Integer id, ModelMap modelMap) {
        List<Message> list =
                messageService.getByIdsLegit((User) req.getSession().getAttribute("user"), id);
        modelMap.addAttribute("messages", list);
        modelMap.addAttribute("receiver_id", id);
        return "messages";
    }
}
