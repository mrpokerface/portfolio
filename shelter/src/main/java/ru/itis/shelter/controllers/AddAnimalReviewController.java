package ru.itis.shelter.controllers;

import ru.itis.shelter.models.AnimalReview;
import ru.itis.shelter.models.UserReview;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.AnimalReviewService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/reviewanimal")
public class AddAnimalReviewController {

    @Autowired
    private AnimalReviewService animalReviewService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(HttpServletRequest req) {
        String reviewedId = req.getQueryString().split("=")[1];
        AnimalReview review = AnimalReview.builder()
                .authorId(((User)req.getSession().getAttribute("user")).getId())
                .reviewedId(Integer.parseInt(reviewedId))
                .rating(Integer.parseInt(req.getParameter("rating")))
                .text(req.getParameter("text"))
                .build();
        animalReviewService.addReview(review);
        return "redirect:/animal/" + reviewedId;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage() {
        return "review_animal";
    }
}
