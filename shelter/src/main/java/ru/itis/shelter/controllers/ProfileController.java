package ru.itis.shelter.controllers;

import ru.itis.shelter.models.Animal;
import ru.itis.shelter.models.UserReview;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import ru.itis.shelter.services.AnimalService;
import ru.itis.shelter.services.CookieService;
import ru.itis.shelter.services.UserReviewService;
import ru.itis.shelter.services.UserService;

import javax.servlet.http.*;
import java.util.List;
import java.util.Optional;

@Controller
public class ProfileController {

    @Autowired
    private UserService userService;
    @Autowired
    private CookieService cookieService;
    @Autowired
    private UserReviewService userReviewService;
    @Autowired
    private AnimalService animalService;

    @RequestMapping("/profile")
    public String doPost(HttpServletRequest req, HttpServletResponse res) {
        HttpSession session = req.getSession();
        session.setAttribute("user", null);
        Cookie remember = cookieService.getRememberMeCookie(req.getCookies());
        if (remember != null) {
            remember.setMaxAge(0);
            res.addCookie(remember);
        }
        return "redirect:/login";
    }

    @RequestMapping("/profile/{id}")
    public String doGet(HttpServletRequest req, @PathVariable int id, ModelMap modelMap) {
        req.setAttribute("user", req.getSession().getAttribute("user"));
        List<Animal> animals = animalService.getByOwnerId(id);
        Optional<User> userOptional = userService.getById(id);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            List<UserReview> reviews = userReviewService.getReviewsByUserId(user.getId());
            modelMap.addAttribute("reviews", reviews);
            modelMap.addAttribute("userProfile", user);
            modelMap.addAttribute("animals", animals);
            return "profile";
        } else {
            return "redirect:/posts";
        }

    }
}
