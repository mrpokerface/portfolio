package ru.itis.shelter.controllers;

import ru.itis.shelter.models.UserReview;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.UserReviewService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/reviewuser")
public class AddUserReviewController {

    @Autowired
    private UserReviewService userReviewService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(HttpServletRequest req) {
        User user = (User) req.getSession().getAttribute("user");
        UserReview review = UserReview.builder()
                .text(req.getParameter("text"))
                .rating(Integer.parseInt(req.getParameter("rating")))
                .authorId(user.getId())
                .build();
        int id = Integer.parseInt(req.getQueryString().split("=")[1]);
        userReviewService.addReview(review, id);
        return "redirect:/profile/" + id;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doGet() {
        return "review_user";
    }
}
