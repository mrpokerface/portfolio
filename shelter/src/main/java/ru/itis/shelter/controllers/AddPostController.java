package ru.itis.shelter.controllers;

import ru.itis.shelter.forms.PostForm;
import ru.itis.shelter.models.Animal;
import ru.itis.shelter.models.Post;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.AnimalService;
import ru.itis.shelter.services.PostService;

import javax.servlet.http.HttpServletRequest;
import java.sql.Timestamp;
import java.util.List;

@Controller
@RequestMapping("/addpost")
public class AddPostController {

    @Autowired
    private PostService postService;
    @Autowired
    private AnimalService animalService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(HttpServletRequest req, PostForm form) {
        User user = (User) req.getSession().getAttribute("user");
        Post post = Post.builder()
                .description(form.getDesc())
                .userId(user.getId())
                .start(form.getStartDate())
                .end(form.getEndDate())
                .isFinished(false)
                .build();
        postService.addPost(post);
        return "redirect:/posts";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doGet(HttpServletRequest req) {
        User user = (User) req.getSession().getAttribute("user");
        List<Animal> animals = null;
        if (user != null) {
            animals = animalService.getByOwnerId(user.getId());
        }
        req.setAttribute("animals", animals);
        req.setAttribute("user", user);
        return "add_post";
    }
}
