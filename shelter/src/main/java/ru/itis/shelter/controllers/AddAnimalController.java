package ru.itis.shelter.controllers;

import ru.itis.shelter.models.Animal;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.AnimalService;

import javax.servlet.http.HttpServletRequest;

@Controller
@RequestMapping("/addanimal")
public class AddAnimalController {

    @Autowired
    private AnimalService animalService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(HttpServletRequest req) {
        User user = (User) req.getSession().getAttribute("user");
        Animal animal = Animal.builder()
                .ownerId(user.getId())
                .name(req.getParameter("name"))
                .age(Integer.parseInt(req.getParameter("age")))
                .rating(0.0)
                .build();
        animalService.addAnimal(animal);
        return "redirect:/profile/" + user.getId();
    }

    @RequestMapping(method = RequestMethod.GET)
    public String getPage() {
        return "add_animal";
    }
}
