package ru.itis.shelter.controllers;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.shelter.models.Animal;
import ru.itis.shelter.models.AnimalReview;
import ru.itis.shelter.models.UserReview;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.AnimalReviewService;
import ru.itis.shelter.services.AnimalService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

@Controller
public class AnimalController {

    @Autowired
    private AnimalService animalService;
    @Autowired
    private AnimalReviewService animalReviewService;

    @RequestMapping(value = "/animal/{id}", method = RequestMethod.GET)
    public String doGet(HttpServletRequest req, @PathVariable Integer id, ModelMap modelMap) {
        Animal animal = animalService.getByAnimalId(id);
        List<AnimalReview> reviews = animalReviewService.getReviewsByAnimalId(id);
        User user = (User) req.getSession().getAttribute("user");
        modelMap.addAttribute("animal", animal);
        modelMap.addAttribute("user", user);
        modelMap.addAttribute("reviews", reviews);
        return "animal";
    }
}
