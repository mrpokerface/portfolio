package ru.itis.shelter.controllers;

import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import ru.itis.shelter.models.Post;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.PostService;

import java.util.List;
import java.util.Optional;

@Controller
public class PostsController {

    @Autowired
    private PostService postService;

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.POST)
    public String doPost(@PathVariable Integer id) {
        Post post = postService.getPost(id).get();
        post.setFinished(true);
        postService.markAsDone(post);
        return "redirect:/posts";
    }

    @RequestMapping(value = "/posts", method = RequestMethod.GET)
    public String getPosts(ModelMap modelMap) {
        List<Post> posts = postService.getAllPosts();
        modelMap.addAttribute("posts", posts);
        return "posts";
    }

    @RequestMapping(value = "/posts/{id}", method = RequestMethod.GET)
    public String getPost(@PathVariable Integer id, ModelMap modelMap) {
        Optional<Post> postOptional = postService.getPost(id);
        if (postOptional.isPresent()) {
            modelMap.addAttribute("post", postOptional.get());
            return "post";
        } else {
            return "redirect:/posts";
        }
    }
}
