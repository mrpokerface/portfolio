package ru.itis.shelter.controllers;

import ru.itis.shelter.forms.LoginForm;
import ru.itis.shelter.models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.services.UserService;

import javax.servlet.http.*;
import java.util.Optional;

@Controller
@RequestMapping("/login")
public class LoginController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(LoginForm form, HttpServletRequest req) {
        HttpSession session = req.getSession();
        Optional<User> userOptional = userService.authenticate(form);
        if (userOptional.isPresent()) {
            User user = userOptional.get();
            session.setAttribute("user", user);
            return "redirect:/posts";
        } else {
            return "redirect:/login";
        }
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doGet(HttpServletRequest req) {
        HttpSession session = req.getSession();
        if (session.getAttribute("user") == null) {
            return "login";
        } else {
            return "redirect:/posts";
        }
    }
}
