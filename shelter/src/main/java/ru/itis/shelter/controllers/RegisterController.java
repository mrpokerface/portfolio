package ru.itis.shelter.controllers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import ru.itis.shelter.forms.RegisterForm;
import ru.itis.shelter.services.UserService;

@Controller
@RequestMapping("/register")
public class RegisterController {

    @Autowired
    private UserService userService;

    @RequestMapping(method = RequestMethod.POST)
    public String doPost(RegisterForm form) {
        userService.addUser(form);
        return "redirect:/login";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String doGet() {
        return "register";
    }
}
