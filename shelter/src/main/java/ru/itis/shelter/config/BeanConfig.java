package ru.itis.shelter.config;

import com.google.gson.Gson;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ViewResolver;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.view.freemarker.FreeMarkerConfigurer;
import org.springframework.web.servlet.view.freemarker.FreeMarkerViewResolver;
import ru.itis.shelter.repositories.*;
import ru.itis.shelter.services.*;

@Configuration
@PropertySource("classpath:application.properties")
@EnableWebMvc
@ComponentScan
public class BeanConfig {

    @Value("${datasource.url}")
    private String URL;

    @Value("${datasource.username}")
    private String username;

    @Value("${datasource.password}")
    private String password;

    @Value("${datasource.class}")
    private String classname;

    @Bean
    public PostsRepositoryHibernateImpl postsRepositoryHibernate() {
        return new PostsRepositoryHibernateImpl();
    }

    @Bean
    public UsersRepositoryHibernateImpl usersRepositoryHibernate() {
        return new UsersRepositoryHibernateImpl();
    }

    @Bean
    public UserReviewRepositoryHibernateImpl userReviewRepositoryHibernate() {
        return new UserReviewRepositoryHibernateImpl();
    }

    @Bean
    public AnimalReviewRepositoryHibernateImpl animalReviewRepositoryHibernate() {
        return new AnimalReviewRepositoryHibernateImpl();
    }

    @Bean
    public AnimalRepositoryHibernateImpl animalRepositoryHibernate() {
        return new AnimalRepositoryHibernateImpl();
    }

    @Bean
    public Gson gson() {
        return new Gson();
    }

    @Bean
    public ViewResolver viewResolver() {
        FreeMarkerViewResolver viewResolver = new FreeMarkerViewResolver();
        viewResolver.setCache(true);
        viewResolver.setPrefix("");
        viewResolver.setSuffix(".ftl");
        return viewResolver;
    }

    @Bean
    public FreeMarkerConfigurer freemarkerConfig() {
        FreeMarkerConfigurer freeMarkerConfigurer = new FreeMarkerConfigurer();
        freeMarkerConfigurer.setTemplateLoaderPath("WEB-INF/classes/templates/");
        return freeMarkerConfigurer;
    }

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    @Bean
    public JdbcTemplate jdbcTemplate() {
        return new JdbcTemplate(dataSource());
    }

    @Bean
    protected BCryptPasswordEncoder encoder() {
        return new BCryptPasswordEncoder();
    }

    @Bean
    protected DriverManagerDataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(classname);
        dataSource.setUrl(URL);
        dataSource.setPassword(password);
        dataSource.setUsername(username);
        return dataSource;
    }

    @Bean
    public AnimalService animalService() {
        return new AnimalService();
    }

    @Bean
    public AnimalReviewService animalReviewService() {
        return new AnimalReviewService();
    }

    @Bean
    public CookieService cookieService() {
        return new CookieService();
    }

    @Bean
    public MessageService messageService() {
        return new MessageService();
    }

    @Bean
    public PostService postService() {
        return new PostService();
    }

    @Bean
    public UserReviewService userReviewService() {
        return new UserReviewService();
    }

    @Bean
    public UserService userService() {
        return new UserService();
    }

    @Bean
    public MessageRepositoryImpl messageRepository() {
        return new MessageRepositoryImpl(dataSource());
    }

}
