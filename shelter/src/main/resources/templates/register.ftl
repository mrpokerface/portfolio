<html >
<head>
  <meta charset="UTF-8">
  <title>Register</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="../resources/css/reg_auth_red.css">

  
</head>
<body>
<div class="wrapper">
  <div class="header">
    <h3 class="sign-in">Register</h3>
  </div>
  <div class="clear"></div> 
  <form method="post">
      <div>
        <input class="user-input" type="text" name="username" id="username" required placeholder="Username"  />
      </div>
      <div>
          <input class="user-input" type="text" name="email" id="email" required placeholder="E-mail"  />
      </div>
      <div>
        <input type="password" name="password" id="password" required  placeholder="Password"/>
      </div>
      <div>
        <input type="text" name="name" id="name" required  placeholder="Name"/>
      </div>
      <div>
        <input type="text" name="surname" id="surname" required  placeholder="Surname"/>
      </div>
      <input type="submit" value="Register" />

  </form>
</div>
</body>
</html>