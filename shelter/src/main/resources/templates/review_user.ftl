<html>
<head>
    <title>Review the user</title>
    <link rel="stylesheet" type="text/css" href="../resources/css/add_new.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="contact-form">
    <div class="container">
        <form method="post">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h1>Review</h1>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 right">
                    <div class="form-group">
                        <textarea class="form-control form-control-lg" placeholder="Text" name="text"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="number" min="1" max="5" step="1"
                               class="form-control form-control-lg" placeholder="Rating" name="rating">
                    </div>

                    <input type="submit" class="btn btn-secondary btn-block" value="Review!">
                </div>

            </div>
        </form>
    </div>
</div>
</body>
</html>