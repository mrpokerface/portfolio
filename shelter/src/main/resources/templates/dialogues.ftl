<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Dialogues</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="../resources/css/one_page_forum.css" rel="stylesheet">
</head>
<div class="class_almaz">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand">Shelter</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/posts">Posts</a>
                </li>

                <#if user??>
                <li class="nav-item">
                    <a class="nav-link" href="/profile/${user.id}">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/addpost">Add new</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/addanimal">Add animal</a>
                </li>
                <#else>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Profile</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Add new</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/login">Add animal</a>
                </li>
                </#if>
            </ul>
        <#if user??>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/profile/${user.id}" class="nav-link"><span class="fa fa-user-plus"></span>${user.login}</a></li>
            </ul>
        <#else>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/register" class="nav-link"><span class="fa fa-user-plus"></span>Sign Up</a></li>
                <li><a href="/login" class="nav-link"><span class="fa fa-sign-in-alt"></span>Log in</a></li>
            </ul>
        </#if>

        </div>
    </nav>
</div>
<body class="bg-light">
<h3 class="border-bottom border-gray pb-2 mb-0">dialogues</h3>

<#list dialogues as dialogue>
        <div class="media text-muted pt-3">
            <img data -src="holder.js/32x32?theme=thumb&bg=007bff&fg=007bff&size=1" alt="" class="mr-2 rounded">
            <a href="/messages/${dialogue.id}">
                <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                    <strong class="d-block text-gray-dark">${dialogue.name}</strong>
                </p>
            </a>
        </div>
</#list>
<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
        integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
        crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
<script src="../assets/js/vendor/popper.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"
        integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWcVRZkx5xU3daNlYb49AxTl"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
        integrity="sha384-ChfqqxuZUCnJSK3MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
        crossorigin="anonymous"></script>
<script src="../resources/js/one_page_forum.js"></script>
</body>
</html>
