<html>
<head>
    <title>Add post</title>
    <link rel="stylesheet" type="text/css" href="../resources/css/add_new.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="contact-form">
    <div class="container">
        <form method="post">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h1>Add new post</h1>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 right">
                    <div class="form-group">
                        <label for="animal">Animal:</label>
                        <select id="animal" name="animal">
                            <#list animals as animal>
                                <option value="${animal.id}">${animal.name}</option>
                            </#list>
                        </select>
                    </div>
                    <div class="form-group">
                        <textarea class="form-control form-control-lg" placeholder="Description" name="desc"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="startDate">Start day:</label>
                        <input type="date" id="startDate" name="startDate">
                    </div>
                    <div class="form-group">
                        <label for="endDate">End day:</label>
                        <input type="date" id="endDate" name="endDate">
                    </div>
                    <input type="submit" class="btn btn-secondary btn-block" value="Ask for help!">
                </div>

            </div>
        </form>
    </div>
</div>
</body>
</html>