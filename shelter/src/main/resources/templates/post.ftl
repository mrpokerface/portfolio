<!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">


    <title>Post</title>

    <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Custom styles for this template -->
    <link href="https://fonts.googleapis.com/css?family=PlayfairDisplay:700,900" rel="stylesheet">
    <link href="../resources/css/one_item.css" rel="stylesheet">
</head>
<body>
<div class="class_almaz">
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand">Shelter</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarCollapse"
                aria-controls="navbarCollapse" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarCollapse">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="/posts">Posts</a>
                </li>
                    <#if user??>
                        <li class="nav-item">
                            <a class="nav-link" href="/profile/${user.id}">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/addpost">Add post</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/addanimal">Add animal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/messages">Messages</a>
                        </li>
                    <#else>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Profile</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Add new</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Add animal</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="/login">Messages</a>
                        </li>
                    </#if>
            </ul>
            <#if user??>
            <a class="nav-link"><span class="fa fa-user-plus"></span>
                ${user.login}
            </a>
            <#else>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="/register" class="nav-link">
                    <span class="fa fa-user-plus"></span>Sign Up</a></li>
                <li><a href="/login" class="nav-link">
                    <span class="fa fa-sign-in-alt"></span>Log in</a></li>
            </ul>
            </#if>
        </div>
    </nav>
</div>

<div class="container">
    <main role="main" class="container">
        <div class="row">
            <div class="col-md-8 blog-main">
                <div class="blog-post">
                    <p>${post.description}</p>
                </div>
            </div>
            <aside class="col-md-4 blog-sidebar">
                <div class="p-3 mb-3 bg-light rounded">"
                    <#if (user??) && (user.id == post.userId)>
                    <a href="/messages/${user.id}"><h4 class="font-italic">Contact</h4></a>
                    </#if>
                    <br>
                </div>
            </aside>
        </div>
    </main>
    <#if (user??) && (user.id == post.userId)>
    <form method="post" action="/posts/${post.id}">
        <input type="hidden" name="done" value="true">
        <input type="submit" class="btn btn-secondary btn-block" value="Done">
    </form>
    </#if>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js"
            integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN"
            crossorigin="anonymous"></script>
    <script>window.jQuery || document.write('<script src="../assets/js/vendor/jquery-slim.min.js"><\/script>')</script>
    <script src="../assets/js/vendor/popper.min.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.bundle.min.js"
            integrity="sha384-pjaaA8dDz/5BgdFUPX6M/9SUZv4d12SUPF0axWcVRZkx5xU3daNlYb49AxTl"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"
            integrity="sha384-ChfqqxuZUCnJSK3MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy"
            crossorigin="anonymous"></script>

</body>
</html>
