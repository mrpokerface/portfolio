<html>
<head>
    <title>Add animal</title>
    <link rel="stylesheet" type="text/css" href="../resources/css/add_new.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
          integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
</head>
<body>
<div class="contact-form">
    <div class="container">
        <form method="post">
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <h1>Add your animal</h1>
                </div>

                <div class="col-lg-8 col-md-8 col-sm-12 right">
                    <div class="form-group">
                        <textarea class="form-control form-control-lg" placeholder="Name" name="name"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="number" class="form-control form-control-lg" placeholder="age" name="age">
                    </div>
                    <input type="submit" class="btn btn-secondary btn-block" value="Add an animal">
                </div>

            </div>
        </form>
    </div>
</div>
</body>
</html>