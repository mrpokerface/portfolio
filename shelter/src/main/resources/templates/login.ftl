<html >
<head>
  <meta charset="UTF-8">
  <title>Sign In</title>
  
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/meyer-reset/2.0/reset.min.css">

  <link rel='stylesheet prefetch' href='http://maxcdn.bootstrapcdn.com/font-awesome/4.1.0/css/font-awesome.min.css'>

      <link rel="stylesheet" href="../resources/css/reg_auth_red.css">

  
</head>
<body>
<div class="wrapper">
  <div class="header">
    <h3 class="sign-in">Sign in</h3>
  </div>
   <div class="clear"></div> 
  <form method="post">
      <div>
        <input class="user-input" type="text" name="username" id="username" required placeholder="Username"  />
      </div> 
      <div>
        <input type="password" name="password" id="password" placeholder="Password"/>
      </div>
     <div>
      <input type="submit" value="Sign in" />
    </div>
    <div class="radio-check">   
      <input type="radio" class="radio-no" id="no" name="remember" value="yes">
      <label for="no"><i class="fa fa-times"></i></label>  
      <input type="radio" class="radio-yes" id="yes" name="remember" value="no" checked>
      <label for="yes"><i class="fa fa-check"></i></label>
      <span class="switch-selection"></span>
    </div>
      <span class="check-label">Remember me</span>
      <div class="clear"></div>
  </form>  
</div>
  <script src='http://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js'></script>
</body>
</html>
